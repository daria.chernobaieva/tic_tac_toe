# tic_tac_toe

That is the script for the tic-tac-toe game, CLI-based. 

## Getting started

 - [ ] Clone repository and start the script
```
cd <repo_path>
python main.py
```

 - [ ] Make a move for player 1. In suggested input set your move as a combination <row>,<column>. 
Both can be set in range [1, 2, 3]. For example, `1,1`, `2,3`, etc.
 - [ ] Make a move for player 2. 
 - [ ] When the game is over you can continue with choosing `Yes` or `No` answer to the question: `Do you want to start a new game?`



## Tic Tac Toe Rules

 1. The game is played on a grid that's 3 squares by 3 squares.

2. You are X, your friend (or the computer in this case) is O. Players take turns putting their marks in empty squares.

3. The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.

4. When all 9 squares are full, the game is over. If no player has 3 marks in a row, the game ends in a tie. 


### That's it. Hope you'll enjoy the game!

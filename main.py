# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import itertools
import random


class WrongMoveError(Exception):
    pass


class Cell:
    """Cell object"""
    def __init__(self, a, b):
        self.row = a
        self.column = b
        self.occupied = False
        self.occupied_by = None

    def __repr__(self):
        return f'Cell: {self.row}, {self.column}'

    def occupy(self, by):
        """Occupy the cell as a result of the move"""
        if self.occupied:
            raise WrongMoveError(f'Cell {self} is already occupied!')

        self.occupied = True
        self.occupied_by = by

    def display(self):
        """Display cell"""
        return self.occupied_by if self.occupied else ' '


class Board:
    """Board object"""
    def __init__(self, size=3):
        self.size = size
        self.content = []

        for i in range(1, self.size + 1):
            row = []
            for j in range(1, self.size + 1):
                row.append(Cell(i, j))
            self.content.append(row)

        self.flat_content = list(itertools.chain(*self.content))
        self.current_user = random.choice(('X', '0'))

    def move(self):
        """User's move"""
        try:
            row, column = tuple(map(int, input(f'Input user {self.current_user}: ').split(', '.strip())))
        except Exception:
            raise WrongMoveError(
                'Wrong input format. Please, be sure your input is in a format: row,column. For example: 1,1')

        if any(ch_ not in range(1, self.size + 1) for ch_ in (row, column)):
            raise WrongMoveError(f'Cell row/column should be in the range {list(range(1, self.size + 1))}')

        cell = self.content[row - 1][column - 1]
        cell.occupy(self.current_user)
        return cell

    def display(self):
        """Display current state of the board"""
        for i, row in enumerate(self.content):
            if i == 0:
                print('    ' + '   '.join([str(j + 1) for j, _ in enumerate(row)]))
            print('  ' + '+---' * self.size + '+')
            print(f'{i + 1} | ' + ' | '.join([cell_.display() for cell_ in row]) + ' |')
        print('  ' + '+---' * self.size + '+')

    def check_winner(self, current_cell):
        """Check for winning condition"""
        current_value = current_cell.display()
        row = [cell_ for cell_ in self.flat_content if cell_.row == current_cell.row]
        column = [cell_ for cell_ in self.flat_content if cell_.column == current_cell.column]
        diagonal = [cell_ for cell_ in self.flat_content if
                    abs(cell_.row - current_cell.row) == abs(cell_.column - current_cell.column)]

        return all(
            cell_.display() == current_value for cell_ in row
        ) or all(
            cell_.display() == current_value for cell_ in column
        ) or all(
            cell_.display() == current_value for cell_ in diagonal
        )

    def switch_user(self):
        """Switch user"""
        self.current_user = {
            'X': '0',
            '0': 'X'
        }.get(self.current_user)


def start_game():
    """Start a new game"""
    # Use a breakpoint in the code line below to debug your script.
    board = Board()
    print(f'Board of size {board.size}x{board.size} is ready. Please, set your move in a format: row,column. For example: 1,1')

    while not all(cell.occupied for cell in board.flat_content):
        try:
            current_cell = board.move()
            if board.check_winner(current_cell=current_cell):
                print(f'User {board.current_user} won!')
                board.display()
                break
            board.switch_user()
            board.display()
        except WrongMoveError as e:
            print(e)

    print('Game over!')
    continue_ = {
        'Yes': True,
        'No': False
    }.get(input('Do you want to start a new game? (Yes/No): '), False)
    if continue_:
        start_game()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    start_game()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
